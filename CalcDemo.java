package calculator;

import java.util.Scanner;

public class CalcDemo {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String task = reader.nextLine();
        String[] parts = task.split(" ");

        double a = Double.parseDouble(parts[0]);
        double b = Double.parseDouble(parts[2]);
        int d = 0;
        switch (parts[1]) {
            case "+":
                System.out.println("a + b" + Calc.add(a, b));

            case "-":
                System.out.println("a - b" + Calc.difference(a, b));

            case "*":
                System.out.println("a * b" + Calc.composition(a, b));

            case "/":
                System.out.println("a / b" + Calc.real(a, b));
                while (d == 0) {
                    System.out.println("Данные введены некорректно, попробуйте снова.");
                }

            case "\\":
                System.out.println("a / b" + Calc.integer(a, b));
                while (d == 0) {
                    System.out.println("Данные введены некорректно, попробуйте снова.");
                }

            case "%":
                System.out.println("a % b" + Calc.balance(a, b));
                while (d == 0) {
                    System.out.println("Данные введены некорректно, попробуйте снова.");
                }

            case "^":
                System.out.println("a ^ b" + Calc.pow(a, b));

                break;

        }
    }
}


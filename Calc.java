package calculator;

public class Calc {
    /**
     * Метод возвращает сумму 2-х чисел.
     *
     * @param a первая переменная
     * @param a вторая переменная
     * @return a+b
     */
    public static double add(double a, double b) {
        return a + b;
    }

    /**
     * Метод возвращающий разность 2-х чисел.
     *
     * @param a
     * @param b
     * @return a-b
     */
    public static double difference(double a, double b) {
        return a - b;
    }

    /**
     * Метод для умножения 2-х чисел.
     *
     * @param a
     * @param b
     * @return a*b
     */
    public static double composition(double a, double b) {
        return a * b;
    }

    /**
     * Метод производящий вещественное деление.
     *
     * @param a
     * @param b
     * @return a/b
     */
    public static double real(double a, double b) {
        return a / b;
    }

    /**
     * Метод производящий целочисленное деление.
     *
     * @param a
     * @param b
     * @return a/b
     */
    public static int integer(double a, double b) {
        return (int) (a / b);
    }

    /**
     * Метод производящий деление и выведения остатка.
     *
     * @param a
     * @param b
     * @return a/b
     */
    public static double balance(double a, double b) {
        return a % b;
    }

    /**
     * Метод производящий вычисление числа a в b степень.
     *
     * @param a
     * @param b
     * @return a/b
     */
    public static double pow(double a, double b) {
        double result = 1;
        for (int i = 0; i < b; i++) {
            result = (result * a);
        }
        return result;
    }

}

